@extends('layout')

@section('title', 'Сообщения')

@section('content')
    <a class="btn btn-primary" role="button" href="{{ route('users.create') }}">Добавить сообщение</a>

    @foreach($users as $user)
        <div class="card mt-3">
            <div class="card-header">
                {{ $user->name }}
            </div>
            <ul class="list-group list-group-flush">
                <li class="list-group-item">{{ $user->phone }}</li>
                <li class="list-group-item">{{ $user->polyclinic }}</li>
                <li class="list-group-item">{{ $user->theme }}</li>
                <li class="list-group-item">{{ $user->message }}</li>
            </ul>
        </div>
    @endforeach
@endsection
