@extends('layout')
@section('title', 'Создать сообщение')

@section('content')
    @if(isset($messageWarning))
        <h4 class="text-danger">{{ $messageWarning }}</h4>
    @endif
    <a class="btn btn-primary mt-3 mb-3" role="button" href="{{ route('users.index') }}">Список сообщений</a>
    <form method="POST" action="{{ route('users.store') }}" class="row g-3">
        @csrf
        <div class="col-12">
            <label for="inputName" class="form-label">ФИО</label>
            <input name="name" value="{{ old('name') }}" type="text" class="form-control" id="inputName"
                   placeholder="Введите ФИО">
            @error('name')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="col-12">
            <label for="inputPhone" class="form-label">Телефон</label>
            <input name="phone" value="{{ old('phone') }}" type="text" class="form-control" id="inputPhone"
                   placeholder="+7 000 000-00-00">
            @error('phone')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="col-12">
            <label for="inputPolyclinic" class="form-label">Поликлиника</label>
            <input name="polyclinic" value="{{ old('polyclinic') }}" type="text" class="form-control"
                   id="inputPolyclinic" placeholder="Название поликлиники">
            @error('polyclinic')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="col-12">
            <label class="form-label" for="inputTheme">Тема</label>
            <select name="theme" class="form-select" id="inputTheme">
                <option selected>Выберете...</option>
                <option value="Запрос на изменение" {{ old('theme') == 'Запрос на изменение' ? 'selected' : '' }}>Запрос
                    на изменение
                </option>
                <option value="Инцидент" {{ old('theme') == 'Инцидент' ? 'selected' : '' }}>Инцидент</option>
                <option value="Разработка" {{ old('theme') == 'Разработка' ? 'selected' : '' }}>Разработка</option>
                <option value="Другое" {{ old('theme') == 'Другое' ? 'selected' : '' }}>Другое</option>
            </select>
            @error('theme')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="col-12">
            <label for="inputMessage" class="form-label">Сообщение</label>
            <textarea name="message" class="form-control" id="inputMessage" placeholder="Введите текст" cols="50"
                      rows="5">{{ old('message') }}</textarea>
            @error('message')
            <div class="alert alert-danger">{{ $message }}</div>
            @enderror
        </div>
        <div class="col-12">
            <button type="submit" class="btn btn-primary">Добавить</button>
        </div>
    </form>
@endsection
